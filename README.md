# Py Peertube
This app is for me learning flask, restful apis, html/css and gitlab

**CRUD = Create/Read/Update/Delete**

Login
- not implemented yet: CRUD user accounts

Configuration
- not implemented yet: CRUD user configuration

Data
- not implemented yet: CRUD videos from peertube


## Getting Started

### Prerequisites

#### Step 1 Installation development environment and runtime environment
- sudo apt-get update 
- sudo apt install docker.io
- sudo systemctl start docker
- sudo systemctl enable docker

#### Step 2 Configure settings.ini
minimum: change site, username and password

### Running PyPeerTube

- git checkout 0.0.1
- sudo docker build -t pypeertube:v0.0.1 .
- sudo docker run -d -p 5000:5000 --name pypeertube pypeertube:v0.0.1

## Author
Mario Kuschel - info@satoshishop.de

## License
This project is licensed under the MIT License - see the LICENSE.md file for details

## Support
My bitcoin address 3Aj7QF2DJd49vDpYU6AhZqz8qWKcedpj4z