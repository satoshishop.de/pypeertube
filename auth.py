from requests import post,get

def client(site):
    oauth_url = site + "/api/v1/oauth-clients/local"
    oauth_client = get(oauth_url).json()
    id = oauth_client["client_id"]
    secret = oauth_client["client_secret"]
    return id,secret

def access_token(site,username,password,grant_type="password",response_type="code"):
    """returns oauth token"""
    url = site + "/api/v1/users/token"
    auth_data = {
            "client_id": client(site)[0],
            "client_secret": client(site)[1],
            "grant_type": grant_type,
            "response_type": response_type,
            "username": username,
            "password": password,
        }
    return post(url, data=auth_data).json()
