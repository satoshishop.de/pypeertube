import unittest
from peertube_write import video_metadata, delete_video, channel_id, mimetype, upload_video
from auth import access_token

class TestPeertube_write(unittest.TestCase):
    def setUp(self):
        self.site = "https://peertube.satoshishop.de"
        self.username = "test"
        self.password = "PhY73EMXQJ4mem6bmF8RFUPJAL9vsb"
        self.uuid = "8e635586-afba-4c2e-9156-6808879008d9"
        self.token = access_token(self.site,self.username,self.password)["access_token"]
        self.channelname = "test_channel"
        self.file_location = "./testvideo.mp4"
        self.title = "testupload"

    def tearDown(self):
        pass

    def channel_id(self):
        res = channel_id(self.site, self.channelname)
        self.assertIsInstance(res,int)

    def mimetype(self):
        res = mimetype(self.file_location)
        self.assertIsInstance(res, str)

    def test_upload_video(self):
        res = upload_video(self.site, self.token, self.channelname, self.file_location, self.title)
        res.json().get("video", False)
        self.assertIn("video", res.json())

    def test_video_metadata(self):
        payload = {"tags": ["tag1", "tag2", "tag3"]}
        res = video_metadata(self.site, self.uuid, payload, self.token)
        if res == "":
            self.assertTrue(True)

    def test_delete_video(self):
        uuid = upload_video(self.site, self.token, self.channelname, self.file_location, self.title).json()["video"]["uuid"]
        res = delete_video(self.site, uuid, self.token)
        if res == "":
            self.assertTrue(True)

if __name__ == "__main__":
    unittest.main()





