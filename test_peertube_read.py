import unittest
from os import remove,path
from peertube_read import all_videos_of_account, video_metadata, search_website, download_video

class TestPeertube_read(unittest.TestCase):

    def setUp(self):
        self.site = "https://peertube.satoshishop.de"
        self.username = "test"
        self.password = "PhY73EMXQJ4mem6bmF8RFUPJAL9vsb"
        self.uuid = "8e635586-afba-4c2e-9156-6808879008d9"

    def tearDown(self):
        pass

    def test_all_videos_of_account(self):
        self.assertTrue(all_videos_of_account(self.site, self.username))

    def test_video_metadata(self):
        res = video_metadata(self.site,self.uuid)
        self.assertNotIn("error",res)

    def test_search_website(self):
        self.assertTrue(search_website(self.site,"testvideo"))

    def test_download_video(self):
        if path.exists("./testvideo.mp4"):
            remove("./testvideo.mp4")
        download_video(self.site, self.uuid)
        if path.exists("./testvideo.mp4"):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

if __name__ == "__main__":
    unittest.main()





