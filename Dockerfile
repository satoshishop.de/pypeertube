FROM python:3
MAINTAINER Mario Kuschel info@satoshishop.de
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD [ "python", "./main.py" ]