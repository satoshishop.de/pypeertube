from requests import put, delete, get, post
from mimetypes import guess_type


def video_metadata(site, uuid, payload, token):
    url = "{}/api/v1/videos/{}".format(site, uuid)
    return put(url,
               headers={'Authorization': 'Bearer {0}'.format(token)},
               data=payload
               )


def delete_video(site, uuid, token):
    url = "{}/api/v1/videos/{}".format(site, uuid)
    return delete(url,
                  headers={'Authorization': 'Bearer {0}'.format(token)}
                  )


def channel_id(site, channelname):
    return get('{0}{1}/{2}'.format(site,
                                   '/api/v1/video-channels',
                                   channelname
                                   )).json()['id']


def mimetype(file_location):
    return guess_type(file_location)[0]


def upload_video(site, token, channelname, file_location, title, privacy=1, commentsEnabled=True):
    url = site + "/api/v1/videos/upload"
    with open(file_location, 'rb') as f:
        return post(
            url,
            headers={'Authorization': 'Bearer {0}'.format(token)},
            data={
                'channelId': channel_id(site, channelname),
                'privacy': privacy,
                'commentsEnabled': commentsEnabled,
                'name': title
            },
            files={
                "videofile": (
                    title,
                    f,
                    mimetype(file_location)
                )
            }
        )
