from requests import get
from math import ceil
from time import sleep
from urllib.parse import quote

def all_videos_of_account(site ,username ,delay=0.3):
    url = "{}/api/v1/accounts/{}/videos".format(site,username)
    all_videos_of_account_list = list()
    count = 100
    start = 1

    total_number_of_videos = get(url ,params={
        'count': count,
        'start': start
    }
                                 ).json()["total"]

    max_cycles = ceil(total_number_of_videos / 100)

    for cycle in range(max_cycles):
        sleep(delay)
        res = get(
            url,
            params={
                'count': count,
                'start': start
            }
        )
        all_videos_of_account_list += res.json()["data"]
        start += 100

    return all_videos_of_account_list

def video_metadata(site,uuid,delay=0.3):
    """Pulling metadta for a single video"""
    sleep(delay)
    return get("{}/api/v1/videos/{}".format(site,uuid)).json()

def search_website(site,pattern, find_exact_match=False):
    """returnes videos that match loosly matches the search pattern."""
    """find_exact_match returns the first video that exactly matches the pattern"""
    if isinstance(pattern, str):
        result_list = list()
        search_quote = quote(pattern)
        url = site + "/api/v1/search/videos?start=0&count=10&search={}&sort=-match".format(search_quote)
        results = get(url).json()["data"]
        for result in results:
            if find_exact_match:
                if pattern == result["name"]:
                    if result not in result_list:
                        result_list.append(result)
                        return result_list
            else:
                if result not in result_list:
                    result_list.append(result)
        return result_list

def download_video(site, uuid, path="./"):
    metadata = video_metadata(site, uuid)
    download_url = metadata["files"][0]["fileDownloadUrl"]
    filename = metadata["name"] + download_url[download_url.rfind("."):]
    file = get(download_url)
    if not path[-1:] == "/":
        path += "/"
    f = open(path + filename, 'wb')
    f.write(file.content)
    f.close()
