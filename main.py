#import importlib
from flask import Flask, redirect, url_for, render_template
from flask_compress import Compress  # type: ignore
from flask_cors import CORS  # type: ignore
from flask_talisman import Talisman  # type: ignore

#from os import getenv
from werkzeug.middleware.proxy_fix import ProxyFix
#import logging
import configparser


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1) # type: ignore


# optimization & security
# -----------------------

Compress(app)
CORS(app)
Talisman(
    app,
    force_https=0,
    content_security_policy={
        'default-src': [
                "SELF",
                "https://code.jquery.com", #jquery
                "https://cdn.jsdelivr.net", # popper
                "https://stackpath.bootstrapcdn.com", # bootstrap
            ]
    },
)


@app.route("/")
def home():
    return render_template("index.html")

@app.route("/overview")
def overview():
    return render_template("overview.html")


if __name__ == "__main__":
    # create logger
    # logging.basicConfig(level=logging.INFO)
    # logger = logging.getLogger(__name__)
    # logger.setLevel(logging.DEBUG)

    config = configparser.ConfigParser()
    config.read('settings.ini')

    #set default values if config is empty
    if not config.sections():
        config["site-config"] = {}
        config["site-config"]["path"] = "/path/to/videos"
        config["site-config"]["site"] = "http://example.com"
        config["site-config"]["channel"] = "channelname"
        config["site-config"]["category"] = "2"
        config["site-config"]["licence"] = "7"
        config["site-config"]["language"] = "en"
        config["site-config"]["privacy"] = "1"
        config["site-config"]["nsfw"] = "False"
        config["site-config"]["commentsEnabled"] = "True"
        config["site-config"]["downloadEnabled"] = "True"
        config["site-config"]["support"] = "None"
        config['site-auth'] = {}
        config['site-auth']['username'] = "username"
        config['site-auth']['password'] = "password"

        with open('settings.ini', 'w') as configfile:
            config.write(configfile)

    app.run(host="0.0.0.0", debug=True)